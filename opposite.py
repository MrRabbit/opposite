#!/usr/bin/env python

"""
Usage : <prog> txt
outputs the opposite of txt if found, else, it outputs txt

For example, in kate, you can add an external tool and trigger it from a
 keyboard shortcut.

External tool configuration:
arguments: %{Document:Selection:Text}
output: replace selected text

This way, select text in kate (eg "true") and trigger the script to replace it
by the opposite text (eg. "false").
"""

import sys

# if a word is a substring of another, the longer word must be set first
# in this list to avoid the substring taking precedence.

opp = (
    ["horizontal", "vertical"],
    ["enabled", "disabled"],
    ["pressed", "released"],
    ["enable", "disable"],
    ["press", "release"],
    ["false", "true"],
    ["start", "stop"],
    ["left", "right"],
    ["top", "bottom"],
    ["write", "read"],
    ["high", "low"],
    ["sin", "cos"],
    ["min", "max"],
    ["up", "down"],
    ["yes", "no"],
    ["in", "out"],
    ["on", "off"],
    ["&&", "||"],
    ["0", "1"],
)


def copy_case(dest, src):
    if src.lower() == src:
        return dest.lower()
    elif src.upper() == src:
        return dest.upper()
    else:
        return dest.capitalize()


if len(sys.argv) == 2:
    txt_in = sys.argv[1]
    text_in_generic = txt_in.lower()
    flatten = [
        (elem, group[1 - idx])
        for group in opp
        for idx, elem in enumerate(group)
    ]
    while txt_in:
        for elem, replacement in flatten:
            len_in = len(elem)
            test = txt_in[:len_in].lower()
            if elem == test:
                print(copy_case(replacement, txt_in[:len_in]), end="")
                txt_in = txt_in[len_in:]
                break
        else:
            print(txt_in[0], end="")
            txt_in = txt_in[1:]
