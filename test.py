#!/usr/bin/env python

import unittest
import subprocess


def output(input):
    return subprocess.check_output(['./opposite.py', input]).decode()


class TestIt(unittest.TestCase):

    def test_all(self):
        tests = {
            "0": "1",
            "false": "true",
            "FALSE": "TRUE",
            "False": "True",
            "foo": "foo",
            "001true\nfooFalse": "110false\nfooTrue"
            }
        for base, opposed in tests.items():
            self.assertEqual(output(base), opposed)
            self.assertEqual(output(opposed), base)


unittest.main()
